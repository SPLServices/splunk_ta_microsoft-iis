## Pre-Implementation

* Clean old log information from all servers and ensure retention on server of know more than 7 days. The method of cleanup is at the customer discretion common choices are
	* Customer developed script/service
	* PowerShell Script - [Delete-andor-Archive-IIS](https://gallery.technet.microsoft.com/scriptcenter/Delete-andor-Archive-IIS-e9ccd0cb)
	* VBScript ["Housekeeping script, automatic log locations, logs are just deleted:"](http://www.808.dk/?code-iis-log-housekeeping)

``` vbscript 
Option Explicit
WScript.Timeout = 82800

' This Script deletes IIS log files older than a specified number
' of days.
'
' Run it as a daily scheduled task on high traffic web servers to
' avoid running out of disc space.
'
' Edit the value for intDelAge to set retention times needed on
' the server.
'
' The locations of the IIS log files are found automatically (for this
' to also work on IIS 7.x on Windows Vista, Windows Server 2008 or
' Windows 7, please enable "IIS 6 Metabase Compatibility" aka
' "IIS Metabase and IIS 6 configuration compatibility").

Dim intDelAge
intDelAge = 30
Dim objIIS
Dim objWeb
Dim objIISOuter
Dim objWebOuter
Set objIISOuter = GetObject("IIS://LOCALHOST")
For Each objWebOuter in objIISOuter
  If LCase(objWebOuter.Class) = "iiswebservice" Then
    Set objIIS = GetObject("IIS://LOCALHOST/W3SVC")
    For Each objWeb in objIIS
      If LCase(objWeb.Class) = "iiswebserver" Then
        Call DeleteLogFiles( _
          objWeb.LogFileDirectory & "\W3SVC" & objWeb.Name, _
          intDelAge)
      End If
    Next
  ElseIf LCase(objWebOuter.Class) = "iissmtpservice" Then
    Set objIIS = GetObject("IIS://LOCALHOST/SMTPSVC")
    For Each objWeb in objIIS
      If LCase(objWeb.Class) = "iissmtpserver" Then
        Call DeleteLogFiles( _
          objWeb.LogFileDirectory & "\SMTPSVC" & objWeb.Name, _
          intDelAge)
      End If
    Next
  ElseIf LCase(objWebOuter.Class) = "iisftpservice" Then
    Set objIIS = GetObject("IIS://LOCALHOST/MSFTPSVC")
    For Each objWeb in objIIS
      If LCase(objWeb.Class) = "iisftpserver" Then
        Call DeleteLogFiles( _
          objWeb.LogFileDirectory & "\MSFTPSVC" & objWeb.Name, _
          intDelAge)
      End If
    Next
  End If
Next

Set objIIS = nothing
Set objIISOuter = nothing

Function DeleteLogFiles(strLogPath, intDelAge)
  Dim objFs
  Dim objFolder
  Dim objSubFolder
  Dim objFile
  Dim objWShell
  Set objWShell = CreateObject("WScript.Shell")
  Set objFs = CreateObject("Scripting.FileSystemObject")
  If Right(strLogPath, 1) <> "\" Then
    strLogPath = strLogPath & "\"
  End If
  If objFs.FolderExists(strLogPath) Then
    Set objFolder = objFs.GetFolder(strLogPath)
      For Each objSubFolder in objFolder.subFolders
        DeleteLogFiles strLogPath & objSubFolder.Name, intDelAge
      Next
      For Each objFile in objFolder.Files
        If (InStr(objFile.Name, "ex") > 0) _
          And (Right(objFile.Name, 4) = ".log") Then
          If DateDiff("d",objFile.DateLastModified,Date) > intDelAge Then
            objFs.DeleteFile(strLogPath & objFile.Name)
          End If
        End If
      Next
    Set objFs = Nothing
    Set objFolder = Nothing
    Set objWShell = nothing
  End If
End Function
```

* Ensure the IIS log configuration has been updated to include at least the following information
``date time s-ip s-computername s-sitename cs-method cs-version cs-host cs-uri-stem cs-uri-query s-port cs-username c-ip sc-status sc-substatus sc-win32-status time-taken sc-bytes cs-bytes cs(User-Agent) cs(Referer) X-Forwarded-For s-contentpath ``

* If an external load balancer is providing ``x-forward*`` information review the following article and ensure ``X-FORWARDED-FOR`` is assigned to ``ClientIP``
	* [iis-and-x-forwarded-for-header](http://www.loadbalancer.org/blog/iis-and-x-forwarded-for-header)

## Data Acquisition Procedure Microsoft 2008R2+

The following deployment methodology will collect all data into indexes for internal and external IIS instances, best practice is to define the index based on the application or related group of applications allowing for application of appropriate access controls. Starting with this configuration is often appropriate while being aware there will be a need to refine the configuration at some appropriate time in the future. 

* Deployment Servers
	* Stage the following apps to deployment-apps
		* ``Splunk_TA_microsoft-iis``
		* ``Splunk_TA_microsoft-iis_seckit_0_auto_inputs``
		* ``Splunk_TA_microsoft-iis_seckit_0_default_inputs``
		* ``Splunk_TA_microsoft-iis_seckit_1_autoext_inputs``
		* ``Splunk_TA_microsoft-iis_seckit_1_defaultext_inputs``
* Update ``SecKit_all_deploymentserver_3_iis/local/serverclass.conf`` define the ``whitelist.0`` to capture hosts with the IIS role servicing internal clients

``` text
[serverClass:seckit_all_3_os_windows_0_iisauto]
whitelist.0 = ^-
```
* Update ``SecKit_all_deploymentserver_3_iis/local/serverclass.conf`` define the ``whitelist.0`` to capture hosts with the IIS role servicing external clients

``` text 
[serverClass:seckit_all_3_os_windows_1_iisauto_ext]
whitelist.0 = ^-
```